# Introduction

Beaucoup de questions se posent quand on a une idée de logiciel Open Source.
Les idées fusent dans tous les sens et on veut que ça se passe bien dans le meilleurs des mondes (enfin, pas celui de [Huxley](https://fr.wikipedia.org/wiki/Le_Meilleur_des_mondes)).
Cet article veut juste esquisser les grandes questions qu'il faudra résoudre.
Il ne se veut pas exhaustif mais se base sur une expérience personnelle des projets Open Source.

Ce petit guide a été écrit initialement pour les étudiants de l'[EFREI](http://efrei.fr/) qui découvrent le monde libre avec le [LUG](https://fr.wikipedia.org/wiki/Groupe_d%27utilisateurs_Linux) [Efrei-Linux](https://www.efrei-linux.fr/).

# Etat des lieux

La question pourrait paraitre à premier abord déprimante car il n'est pas rare d'avoir une super idée et de s'apercevoir après quelques recherches qu'elle existe déjà ... et en encore mieux ...

Faites des recherches sur ce qui existe déjà, ça vous fera une idée des problématiques existantes et comment vous pouvez les dépasser.
Ca vous fera au moins prendre du recul sur votre projet et même si votre idée de projet existe déjà, peut-être qu'elle n'a pas été fait d'une certaine manière qui pourrait apporter un gros plus.

Autrement, pourquoi pas contribuer sur un projet qui existe déjà et participer aux efforts d'une communauté ?
Il peut être très tentant de ré-inventer la roue mais la plupart du temps, il vaut mieux faire évoluer un projet.
C’est l'une des principales difficultés qu’ont les novices en développement: rentrer dans un projet et le comprendre.
Une solution peut aussi être le [fork](https://fr.wikipedia.org/wiki/Fork_(d%C3%A9veloppement_logiciel)) quand le projet n’a plus personne dedans ou quand les développement vont trop faire changer le logiciel initial .

C'est ce qu'il s'est passé par exemple avec le projet [Jirafeau](http://jirafeau.net/): le projet à fork pour ajouter des fonctionnalités à [Jyraphe](http://home.gna.org/jyraphe/).

Trop de [bons projets libres](https://framasoft.org/rubrique2.html) existent et tous ont besoin d'aide pour en faire quelque chose d'encore mieux.

# Portée du projet

Comme tous les projets, il est plutôt sain de définir ce que doit faire le projet au minimum de façon informelle.

Ouvrez simplement un fichier `TODO.md` et décrivez en quelques mots chaque fonctionnalité (notez que le format [Markdown](https://en.wikipedia.org/wiki/Markdown#Example) est largement utilisé). 
Quand la liste grandie, vous pouvez l'organiser en sections, séparer les idées d'implémentation des idées de fonctionnalité.
Il faut ici se concentrer sur les quelques principales fonctionnalité clefs du projet et se dire: qu'est-ce que fera le projet dans la toute première version alpha ? Qu'est-ce qui peut être réalisé comme évolution plus tard ?

Il est aussi important de définir les limites du projet, ce qu'il ne *DOIT PAS* faire.

Ce détail est important dans tous les projets participatifs: Voulez vous que chacun apporte ses milliers de petites/grosses fonctionnalité ou voulez vous restreindre le projet à ce que vous voulez voir ?

Certains vont vouloir que le projet acquière un maximum de fonctionnalités.
Ce n'est pas forcément un problème de pouvoir ajouter des milliers de "feature" à un projet avec des contributions de partout mais il faut le prévoir assez tôt dans l'architecture et l'organisation du projet.
Pour vous éviter d'être le créateur d'une usine à gaz, optez pour une approche modulaire avec des extensions, de plugins, de modules ou des scripts qui peuvent se greffer au projet, arriver et repartir sans impacter votre coeur de logiciel.

D'autres encore veulent conserver un nombre fixe de fonctionnalités et de dépendances.
Il faudra alors refuser les contributions qui étendent trop le projet.
On retrouve cette volonté dans le [principe KISS](https://fr.wikipedia.org/wiki/Principe_KISS) et bien sur dans la [Philisophie d'Unix](https://fr.wikipedia.org/wiki/Philosophie_d%27Unix): "Écrivez des programmes qui effectuent une seule chose et qui le font bien".

Que vous vouliez faire un projet tentaculaire ou une micro-librairie, signalez votre politique de contribution dans un fichier `CONTRIBUTING.md`.
Cela évitera une certaine déception aux contributeurs qui auront fait un effort peut-être pour rien.

# Conseils de base pour démarrer le projet

Il va falloir avoir une idée des briques qui vont composer votre projet:
- le language ? C ? C++ ? Python ? Rust ? Haskell ?
- Qu'elles librairies allez-vous utiliser ?
- Quel système de construction (build system) ?

Il va falloir faire un petit [proof of concept](https://fr.wikipedia.org/wiki/Preuve_de_concept): tester les librairies, jouer avec, et voir que le projet est réalisable dans ces conditions.

Une fois passé cette étape il faudra éviter un maximum le [YOLO Driven Developpement](https://jaxenter.com/yolo-driven-development-methodology-113469.html).
La bonne idée fondamentale est d'utiliser un gestionnaire de version comme [git](https://git-scm.com/doc).
Prenez vraiment le temps de maitriser cet outil mondialement utilisé qui va vous servir pour tous développement: apprendre à faire un commit, modifier un commit, pousser son commit sur une branche, gérer les branches, gérer les conflits au merge, etc.

Il est aussi conseillé d'adopter un coding style dès les premières lignes surtout si vous avez pas d'auto-formateur pour votre langage. Renseignez vous sur ce qu’il se fait, le [google style](https://google.github.io/styleguide/cppguide.html) et le [Linux Kernel coding style](https://www.kernel.org/doc/Documentation/process/coding-style.rst) sont très utilisés pour respectivement le C++ et le C avec des vérificateurs de code (linter).

Prenez également quelques minutes pour déjà imaginer les tests fonctionnels et/ou les tests unitaires que vous pourriez faire: ([la différence en 10 secondes](https://www.youtube.com/watch?v=0GypdsJulKE)).

Vous devrez ensuite vraiment mettre les mains des le cambouis et réaliser votre première version, retravailler sur les fonctionnalité, les problématiques et les priorités.

# La licence

Et oui, vous l'attendiez... qui dit projet Open Source dit licence Open Source.
Attention, étant une source de débat infinie, le but sera simplement de présenter les licences Open Source les plus utilisées. 
Je fais le distinguo des différentes licences (permissives et libres) pour signaler qu'il y a un choix à faire mais je tiens à laisser le débat idéologique de coté.

## La famille des licences permissives

Celles-ci on été crées dans une logique de _liberté_.

Il s'agit des licences qui vous permettent à n'importe qui de presque tout faire avec un logiciel: le copier, le modifier, le distribuer (même sans divulguer les sources), le commercialiser, etc.
La seule chose que vous ne pouvez généralement pas faire avec est de rendre les créateur responsables de dommages causés par le logiciel et de modifier la licence (créateur inclus).
C'est le cas de la [BSD-3-clause](https://tldrlegal.com/license/bsd-3-clause-license-(revised)), de la [MIT](https://tldrlegal.com/license/mit-license) ou de la [Apache 2.0](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)).

Avec ces licences, il n’y a aucun obstacle légale à l'utilisation et à la modification, même en combinaison avec un logiciel propriétaire. Ce qui implique aussi que le logiciel peut être amélioré/distribué/vendu sans que la communauté ne bénéficie des modifications.

Exemples de projets sous licence permissives:
- [Apache web server](https://httpd.apache.org/) (licence Apache)
- [FreeBSD](https://www.freebsd.org/) (licence BSD)
- [Gitlab OpenSource Edition](https://gitlab.com/) (licence MIT)

## La famille des licences libres/copyleft : 

[Celles-ci](https://tldrlegal.com/licenses/tags/Copyleft) ont été crées dans une logique de [_partage_](https://en.wikipedia.org/wiki/Free_Software_Song#Lyrics).
La notion de "libre" est en references aux [4 libertés fondamentales du logiciel libre](https://www.gnu.org/philosophy/free-sw.html) de [Richard Stallman](https://fr.wikipedia.org/wiki/Richard_Stallman).
Un logiciel Open Source peut donc être libre ... ou non.

Il s'agit principalement de partager les modifications qui ont été apportées pour en faire bénéficier toute la communauté.
Certaines sont plus contraignantes que d'autres mais globalement elle imposent de partager les sources en cas de "distribution".

La [GPLv3](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)), [AGPLv3](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)), la [MPLv2](https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2)) ou la [LGPLv3](https://tldrlegal.com/license/gnu-lesser-general-public-license-v3-(lgpl-3)) sont les plus connues des licences copyleft.

Avec ces licences, le partage des modifications à la communauté devient obligatoire en cas de distribution.
Dans le cas de l’Affero (AGPL), il y a distribution lorsque le logiciel est accessible via un réseau.
Dans certains cas, les licences copyleft peuvent imposer à un logiciel qui les utilisent de distribuer ses sources sous la même licence.

Exemples de projets sous licence libres:
- [Le noyaux Linux](https://kernel.org/): GPLv2
- [Owncloud](https://owncloud.org/): AGPLv3
- [Blender](https://www.blender.org/): GPLv2
- [VLC](https://www.videolan.org/vlc/) LGPLv2
- [Firefox](https://www.mozilla.org/fr/firefox/new/): MPLv2

## Et après ?

Je ne porte pas de jugement sur les licences présentées, chacune a ses avantages et ses inconvénients et c'est à vous de choisir selon vos motivations et le type de projet.
C'est une question qu'il est important de se poser juste avant de rendre le projet publique.
Inutile de vous poser trop de questions tant que vous l'avez pas encore un peu développé.

Et les SOUX dans tout ça ? Que vous fassiez votre projet Open Source avec ça en tête ou non, notez bien qu'aucune des licences Open Sources présentes ici ne vous empêche de vous faire de l'argent tant que vous respectez les conditions de la licence.
Certains projets adoptent même une [double licence](https://www.google.fr/search?q=dual+licensing) voir certains comme comme [Qt](https://www.qt.io/licensing-comparison/) fournissent des version "commerciales" à coté de la version "communautaire" (à condition d'en être l'auteur).
Par ailleurs, de nombreuses sociétés vivent de projets Open Sources en vendant des prestations de services associés (support, hébergement, expertise, temps de développement, etc...) tout en gardant un logiciel 100% libre.

Quelques ressources:
- [tldrlegal](https://tldrlegal.com/): est très sympa pour voir en un coup d'oeil les droits et devoirs de chaque licence.
- [SPDX licence List](https://spdx.org/licenses/): une large liste de licence avec leurs identifiants.
- [GNU licence listing](https://www.gnu.org/licences/license-list.en.html): pour avoir une description de nombreuses licences et connaitre leur compatibilité avec le GPL.
- [creativecommons](https://creativecommons.org/share-your-work/): choisir une licence pour une oeuvre, un écrit, etc.
- Bonus: [opensource.org's Terms of Services](https://opensource.org/ToS): Conditions d'utilisation réutilisables

Petit avertissement: certains seraient tentés de modifier une licence pour compléter selon ses besoins mais sachez que certaines ne vous en donne pas le droit. De façon générale, gardez en tête que se prendre pour un apprenti juriste est une mauvaise idée, surtout quand il n'y a eu aucune jurisprudence sur votre licence.
Préférez donc utiliser des [licences largement utilisées](https://opensource.org/licenses).

Quelque soit la licence utilisée, il faut bien l'appliquer.
Chaque licence à ses directives d'application à consulter auprès de son créateur.
Son application peut passer par ajouter un entête légale en commentaire dans chaque fichier, ajouter le licence complète à la racine du projet, indiquer le "copyright holder" (celui qui détient les droits d'auteur), etc...

# Héberger son projet

Quand vous aurez fait une première version, que vous aurez appliqué la licence et que vous êtes prêt à publier votre projet : pensez à écrire un `README.md` à la racine du projet qui explique tout ce qu'il faut savoir sur le projet.

Les deux plateformes les plus polulaires sont: [Github](http://github.com/) et [Gitlab](http://gitlab.com/). A vous de choisir :)

Commencez par importer votre projets, peut-être ses différentes branches et transformez votre `TODO.md` en tickets/issues sur votre plateforme.

Les plateformes de [CI](https://fr.wikipedia.org/wiki/Int%C3%A9gration_continue) gratuites fleurisses partout: [travis](https://travis-ci.org/), [Gitlab's CI](https://about.gitlab.com/gitlab-ci/), etc.
Elles vous permettront de lancer TOUS les tests à CHAQUE merge request ou montée de version.

Essayez de mettre en place des tests de façon automatisée, une fois que vous avez l'habitude d'en avoir, vous pouvez plus vous en passer et vous les remercierez à chaque fois que votre build aura cassé sans que vous ayez vu l'erreur avant.

Certains projets comme [terraform.io](http://terraform.io/) font passer leurs tests toutes les nuits ce qui produit la création de réelles ressources chez [tous les providers](https://www.terraform.io/docs/providers/index.html).
Bref, les tests, c'est [important](http://lesjoiesducode.fr/post/31452862688/quand-le-stagiaire-me-dit-que-les-tests-cest).

# La communauté

Il y a beaucoup d'exemples de communautés Open Sources et certaines sont énormes.
Celle du noyau Linux comprend [entre 5000 et 6000 developeurs](https://en.wikipedia.org/wiki/Linux_kernel#Developer_community) qui collaborent dans le monde entier.

Toute communauté qui grandie avec des contributeurs réguliers se dote d'un guide de contribution.
Quelques exemples:
- [Linux](https://www.linux.com/publications/how-participate-linux-community)
- [Krita](https://krita.org/en/get-involved/overview/)
- [Terraform.io](https://www.terraform.io/community.html)
- [Owncloud](https://owncloud.org/contribute/)
- [Jirafeau](https://gitlab.com/mojo42/Jirafeau/blob/master/CONTRIBUTING.md)

Toute communauté a ses règles, à vous d'échanger avec les premiers contributeurs réguliers pour définir la meilleurs méthode à adopter que ce soit pour la gestion des branches de développement, les tests, les issues, etc.
Les méthodes et la gestion d'une communauté évolue avec sa taille. Restez simple au début et faites évoluer selon vos besoins d'organisation.
Dans tous les cas, respectez les gens, les contributeurs et gardez un esprit ouvert et diplomate.
Certains projets mettent en place un [code of conduct](https://www.rust-lang.org/en-US/conduct.html) pour prévenir des comportements discriminatoires et sexistes.

Pour commencer, l'organisation la plus simple reste la communication au travers les issues/tickets pour parler du projet et des problèmes rencontrés.
Signalez dans votre `README.md` comment contacter la communauté selon le besoin.
Par exemple pour chater, un [cannal irc](https://webchat.freenode.net/?channels=efreilinux&nick=libriste_en_herbe) fait largement le boulot et pour signaler un problème: un simple ticket/issue en indiquant les  informations necessaires.

# Conclusion

On a survolé pas mal de points, en espérant que cet article ai aidé ses lecteurs à y voir plus clair.

Rappelez vous aussi qu'un un projet Open Source n’est pas forcément un logiciel et peut aussi bien être de l’[art](http://www.artisopensource.net), de la [musique](https://www.jamendo.com/?language=fr), une [cartographie](https://www.openstreetmap.org/), une [traduction collaborative](https://hosted.weblate.org/), une [encyclopédie](http://www.wikipedia.org/), des [objets imprimés](https://www.thingiverse.com/thing:666802) des [robots libres](http://apbteam.org/wiki/Presentation/TechOverview), cet article, etc.

---

Author: Jérôme Jutteau <j.jutteau@gmail.com>

Sources: disponibles sur [https://gitlab.com/mojo42/beginning-open-source-project](https://gitlab.com/mojo42/beginning-open-source-project)

This work is licenced under a [Creative Commons Attribution-ShareAlike 4.0 International licence](http://creativecommons.org/licences/by-sa/4.0/)

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licences/by-sa/4.0/)